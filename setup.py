#!/usr/bin/env python
# encoding: utf-8

import setuptools

with open("README.md", "r") as readme:
    long_description = readme.read()

setuptools.setup(
    name="amr-to-itol",
    version="0.0.5",
    author="Brendan Corey",
    author_email="brendan.w.corey.ctr@mail.mil",
    description="Convert the amr summary reports from MIGHT into an iTOL gene presence/absence dataset file",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mrsn-bio/amr-to-itol-dataset",
    license="GPLv3",
    packages=setuptools.find_packages(),
    package_data={
        "amr_to_itol": ["resources/*.txt"]
    },
    entry_points={'console_scripts': ['amr-to-itol=amr_to_itol.amr_to_itol:main']
                  },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    python_requires='>=3.5',
    zip_safe=False
)
